import { buildApiUrl } from "@/constants";

export function apiRoute(endpoint: string): string {
  return buildApiUrl(endpoint);
}
