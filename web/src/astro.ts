import { URL } from "node:url";

const SERVER_API_BASE_URL = process.env.SERVER_API_BASE_URL;
if (SERVER_API_BASE_URL === undefined || SERVER_API_BASE_URL.length === 0) {
  throw new Error("'SERVER_API_BASE_URL' environment variable must be set.");
}

export function apiRoute(endpoint: string): string {
  return new URL(endpoint, SERVER_API_BASE_URL).href;
}
