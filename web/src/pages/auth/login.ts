import type { APIRoute } from "astro";
import { StatusCodes } from "http-status-codes";
import pWaterfall from "p-waterfall";
import { apiRoute } from "@/astro";
import { LOGIN_PAGE_EXPIRED_SESSION_URL } from "@/constants";

export const post: APIRoute = async function post({ request, redirect }) {
  return await pWaterfall(
    [
      async (req) => await req.formData(),
      async (formData) => {
        const values = Object.fromEntries(formData.entries());
        try {
          return await fetch(apiRoute("/api/auth/login"), {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            redirect: "follow",
            referrerPolicy: "no-referrer",
            credentials: "same-origin",
            body: JSON.stringify(values),
          });
        } catch (error) {
          console.error(error);
          return await Promise.resolve(new Response());
        }
      },
      (apiRes) => {
        if (apiRes.status === StatusCodes.CREATED) {
          const response = redirect("/", StatusCodes.TEMPORARY_REDIRECT);
          Array.from(apiRes.headers.entries()).forEach(([k, v]) =>
            response.headers.set(k, v)
          );

          return response;
        }

        if (
          apiRes.status === StatusCodes.REQUEST_TOO_LONG ||
          apiRes.status == StatusCodes.UNPROCESSABLE_ENTITY
        ) {
          return redirect(
            LOGIN_PAGE_EXPIRED_SESSION_URL,
            StatusCodes.TEMPORARY_REDIRECT
          );
        }

        if (apiRes.status === StatusCodes.UNAUTHORIZED) {
          return redirect(
            LOGIN_PAGE_EXPIRED_SESSION_URL,
            StatusCodes.TEMPORARY_REDIRECT
          );
        }

        return redirect("/500", StatusCodes.TEMPORARY_REDIRECT);
      },
    ],
    request
  );
};
