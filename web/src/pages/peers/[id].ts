import type { APIRoute } from "astro";
import { StatusCodes, ReasonPhrases } from "http-status-codes";
import pWaterfall from "p-waterfall";
import QRCode from "qrcode-svg";
import { apiRoute } from "@/astro";
import { LOGIN_PAGE_EXPIRED_SESSION_URL } from "@/constants";

export const get: APIRoute = async function get({
  params,
  request,
  redirect,
  url,
}) {
  const { id } = params;

  const download = url.searchParams.has("download");

  return await pWaterfall([
    async () => {
      return await fetch(apiRoute(`/api/peers/${id}`), {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Cookie: request.headers.get("cookie") ?? "",
        },
        redirect: "follow",
        referrerPolicy: "no-referrer",
        credentials: "include",
        body: null,
      });
    },
    async (apiRes) => {
      if (apiRes.status === StatusCodes.UNAUTHORIZED) {
        return redirect(
          LOGIN_PAGE_EXPIRED_SESSION_URL,
          StatusCodes.TEMPORARY_REDIRECT
        );
      }

      if (apiRes.status === StatusCodes.NOT_FOUND) {
        return new Response(null, {
          status: StatusCodes.NOT_FOUND,
          statusText: ReasonPhrases.NOT_FOUND,
          headers: apiRes.headers,
        });
      }

      if (apiRes.status !== StatusCodes.OK) {
        throw new UnexpectedServerResponseError();
      }

      const content = await apiRes.text();

      if (download) {
        return new Response(content, {
          status: StatusCodes.OK,
          statusText: ReasonPhrases.OK,
          headers: {
            ...Object.fromEntries(apiRes.headers.entries()),
            "Content-Type": "application/octet-stream",
            "Content-Transfer-Encoding": "binary",
            "Content-Disposition": `attachment; filename="${id}.conf"`,
          },
        });
      }

      const svg = new QRCode(content).svg();
      return {
        body: Buffer.from(svg).toString("base64"),
        headers: {
          ...Object.fromEntries(apiRes.headers.entries()),
          "Content-Type": "image/svg+xml",
        },
      };
    },
  ]).catch((error) => {
    console.error(error);
    if (error instanceof UnexpectedServerResponseError) {
      return new Response(error.message, {
        status: StatusCodes.INTERNAL_SERVER_ERROR,
        statusText: ReasonPhrases.INTERNAL_SERVER_ERROR,
      });
    }

    return new Response("Server error occurred.", {
      status: StatusCodes.INTERNAL_SERVER_ERROR,
      statusText: ReasonPhrases.INTERNAL_SERVER_ERROR,
    });
  });
};

class UnexpectedServerResponseError extends Error {
  public constructor() {
    super("Unexpected response received from the server.");
  }
}
