import { defineConfig, loadEnv } from "vite";

export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd(), "");

  const WEB_API_BASE_URL = env.WEB_API_BASE_URL;
  if (env.WEB_API_BASE_URL === undefined) {
    console.warn(
      "'WEB_API_BASE_URL' environment variable is not set. Assuming a production build."
    );
  }

  const BUILD_API_BASE_URL =
    WEB_API_BASE_URL === undefined
      ? "r => r"
      : `r => new URL(r, ${JSON.stringify(WEB_API_BASE_URL)}).href`;

  return {
    define: {
      __BUILD_API_BASE_URL__: BUILD_API_BASE_URL,
    },
  };
});
