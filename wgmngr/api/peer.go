package api

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/go-jet/jet/v2/mysql"
	"github.com/go-jet/jet/v2/qrm"
	gomysql "github.com/go-sql-driver/mysql"

	m "gitlab.com/wireman/wireman/wgmngr/db/gen/latest/wgmngr/model"
	t "gitlab.com/wireman/wireman/wgmngr/db/gen/latest/wgmngr/table"
	"gitlab.com/wireman/wireman/wgmngr/wg"
)

func (h *Handler) GetActivePeerConfigs(ctx context.Context) ([]byte, error) {
	return wg.ReadActivePeers(ctx, h.db, h.wgServerConf)
}

var (
	ErrPeerAlreadyDeactivated = errors.New("peer has already been deactivated")
	ErrPeerAlreadyActivated   = errors.New("peer has already been activated")
)

func (h *Handler) DeactivatePeer(ctx context.Context, peerID, userID int) error {
	var p m.PeerActivation
	err := t.PeerActivation.
		SELECT(t.PeerActivation.Action).
		WHERE(t.PeerActivation.PeerID.EQ(mysql.Uint32(uint32(peerID)))).
		LIMIT(1).
		ORDER_BY(t.PeerActivation.ID.DESC()).
		QueryContext(ctx, h.db, &p)
	if nil != err && !errors.Is(err, qrm.ErrNoRows) {
		return fmt.Errorf("failed to check for previous deactivation of peer: %v", err)
	} else if p.Action == m.PeerActivationAction_Deactivated {
		return ErrPeerAlreadyDeactivated
	}

	d := m.PeerActivation{
		At:     time.Now(),
		ByID:   uint32(userID),
		PeerID: uint32(peerID),
		Action: m.PeerActivationAction_Deactivated,
	}
	res, err := t.PeerActivation.INSERT(t.PeerActivation.MutableColumns).MODEL(d).Exec(h.db)
	if nil != err {
		mysqlErr := new(gomysql.MySQLError)
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1452 && mysqlErr.SQLState == [5]byte([]byte("23000")) {
			return ErrPeerNotFound
		}

		return fmt.Errorf("failed to insert deactivation record: %v", err)
	}

	rowsNo, err := res.RowsAffected()
	if nil != err {
		return fmt.Errorf("failed to get number of inserted deactivation records: %v", err)
	} else if rowsNo != 1 {
		return fmt.Errorf("expected 1 deactivation record to be inserted, got: %d", rowsNo)
	}

	return h.signalWGRestart(ctx)
}

func (h *Handler) ActivatePeer(ctx context.Context, peerID, userID int) error {
	var p m.PeerActivation
	err := t.PeerActivation.
		SELECT(t.PeerActivation.Action).
		WHERE(t.PeerActivation.PeerID.EQ(mysql.Uint32(uint32(peerID)))).
		LIMIT(1).
		ORDER_BY(t.PeerActivation.ID.DESC()).
		QueryContext(ctx, h.db, &p)
	if nil != err && !errors.Is(err, qrm.ErrNoRows) {
		return fmt.Errorf("failed to check for previous activation of peer: %v", err)
	} else if p.Action == m.PeerActivationAction_Activated {
		return ErrPeerAlreadyActivated
	}

	a := m.PeerActivation{
		At:     time.Now(),
		ByID:   uint32(userID),
		PeerID: uint32(peerID),
		Action: m.PeerActivationAction_Activated,
	}
	res, err := t.PeerActivation.INSERT(t.PeerActivation.MutableColumns).MODEL(a).Exec(h.db)
	if nil != err {
		mysqlErr := new(gomysql.MySQLError)
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1452 && mysqlErr.SQLState == [5]byte([]byte("23000")) {
			return ErrPeerNotFound
		}

		return fmt.Errorf("failed to insert activation record: %v", err)
	}

	rowsNo, err := res.RowsAffected()
	if nil != err {
		return fmt.Errorf("failed to get number of inserted activation records: %v", err)
	} else if rowsNo != 1 {
		return fmt.Errorf("expected 1 activation record to be inserted, got: %d", rowsNo)
	}

	return h.signalWGRestart(ctx)
}
