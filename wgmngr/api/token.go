package api

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/gofrs/uuid"
	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jwt"
)

var (
	AuthTokenExpDuration = time.Hour * 4
)

type TokenClaims struct {
	UserID         int
	Username, Role string
}

func generateToken(secret []byte, claims TokenClaims) (string, error) {
	tokenID, err := uuid.NewV4()
	if nil != err {
		return "", fmt.Errorf("unable to generate token id: %w", err)
	}

	token, err := jwt.NewBuilder().
		Issuer("https://gitlab.com/wireman/wireman").
		IssuedAt(time.Now()).
		Audience([]string{"users"}).
		Expiration(time.Now().Add(AuthTokenExpDuration)).
		JwtID(tokenID.String()).
		NotBefore(time.Now()).
		Subject(fmt.Sprintf("%d", claims.UserID)).
		Claim("username", claims.Username).
		Claim("role", claims.Role).
		Build()
	if nil != err {
		return "", fmt.Errorf("failed to build token: %w", err)
	}

	opts := []jwt.SignOption{}
	serialized, err := jwt.Sign(token, jwa.HS512, []byte(secret), opts...)
	if nil != err {
		return "", fmt.Errorf("unable to sign auth token: %w", err)
	}

	var b strings.Builder
	b.Grow(len(serialized))
	if _, err := b.Write(serialized); nil != err {
		return "", err
	}

	return b.String(), nil
}

func (h *Handler) ParseVerifyToken(token string) (*TokenClaims, error) {
	parseOptions := []jwt.ParseOption{
		jwt.WithVerify(jwa.HS512, h.tokenSecret),
		jwt.WithValidate(true),
		jwt.WithAudience("users"),
		jwt.WithIssuer("https://gitlab.com/wireman/wireman"),
		jwt.WithMinDelta(time.Second*10, jwt.ExpirationKey, jwt.IssuedAtKey),
	}
	parsedToken, err := jwt.Parse([]byte(token), parseOptions...)
	if nil != err {
		return nil, fmt.Errorf("parsing and verifying token failed: %w", err)
	}

	roleClaim, ok := parsedToken.Get("role")
	if !ok {
		return nil, errors.New("role claim expected to exist")
	}
	role, ok := roleClaim.(string)
	if !ok {
		return nil, errors.New("username is expected to be a string")
	}

	usernameClaim, ok := parsedToken.Get("username")
	if !ok {
		return nil, errors.New("role claim expected to exist")
	}
	username, ok := usernameClaim.(string)
	if !ok {
		return nil, errors.New("username is expected to be a string")
	}

	sub := parsedToken.Subject()
	userID, err := strconv.Atoi(sub)
	if nil != err {
		return nil, fmt.Errorf("expected token subject claim to be user id number, got: %s", sub)
	}

	return &TokenClaims{
		UserID:   userID,
		Role:     role,
		Username: username,
	}, nil
}
