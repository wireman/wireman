package api

import (
	"context"
	"errors"
	"fmt"

	"github.com/go-jet/jet/v2/mysql"
	"github.com/go-jet/jet/v2/qrm"

	m "gitlab.com/wireman/wireman/wgmngr/db/gen/latest/wgmngr/model"
	t "gitlab.com/wireman/wireman/wgmngr/db/gen/latest/wgmngr/table"
	"gitlab.com/wireman/wireman/wgmngr/password"
)

type ResetPasswordReq struct {
	UserID          int
	CurrentPassword []byte
	NewPassword     []byte
}

func (h *Handler) ResetPassword(ctx context.Context, req ResetPasswordReq) error {
	var u m.User
	err := t.User.
		SELECT(t.User.Password).
		WHERE(t.User.ID.EQ(mysql.Uint32(uint32(req.UserID)))).
		LIMIT(1).
		QueryContext(ctx, h.db, &u)
	if nil != err {
		if errors.Is(err, qrm.ErrNoRows) {
			return ErrUserNotFound
		}

		return err
	}

	matches, err := password.Compare(u.Password, req.CurrentPassword)
	if nil != err {
		password.Sleep()
		return fmt.Errorf("failed to check current password correctness: %v", err)
	} else if !matches {
		password.Sleep()
		return ErrInvalidCreds
	}

	hash, err := password.Hash(req.NewPassword)
	if nil != err {
		return err
	}

	res, err := t.User.
		UPDATE(t.User.Password).
		SET(hash).
		WHERE(
			mysql.AND(
				t.User.ID.EQ(mysql.Uint32(uint32(req.UserID))),
				mysql.BoolExp(mysql.Func("last_insert_id", t.User.ID)),
			),
		).
		ExecContext(ctx, h.db)
	if nil != err {
		return fmt.Errorf("failed to persist user new password: %v", err)
	}

	lastRecID, err := res.LastInsertId()
	if nil != err {
		return fmt.Errorf("failed to get updated record id: %v", err)
	}
	rowsNo, err := res.RowsAffected()
	if nil != err {
		return fmt.Errorf("failed to get number of rows affected by update: %v", err)
	}
	if rowsNo == 0 && lastRecID == 0 {
		return ErrPeerNotFound
	} else if rowsNo != 1 {
		return fmt.Errorf("expected 1 row of user record to be updated, got %d", rowsNo)
	}

	return nil
}
