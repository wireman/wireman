package api

import (
	"bytes"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"net"
	"net/netip"
	"time"

	"github.com/go-jet/jet/v2/mysql"
	"github.com/go-jet/jet/v2/qrm"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
	"gopkg.in/ini.v1"

	m "gitlab.com/wireman/wireman/wgmngr/db/gen/latest/wgmngr/model"
	t "gitlab.com/wireman/wireman/wgmngr/db/gen/latest/wgmngr/table"
)

var (
	ErrNoMoreIPv4Address = errors.New("no more ipv4 address is available in the range")
	ErrNoMoreIPv6Address = errors.New("no more ipv6 address is available in the range")
)

type CreatePeerReq struct {
	Name        string
	Description string
	ResellerID  int
}

func getLastPeerIPs(ctx context.Context, db *sql.DB) (ipv4 string, ipv6 string, err error) {
	var c m.Peer
	err = t.Peer.SELECT(t.Peer.Ipv4, t.Peer.Ipv6).LIMIT(1).ORDER_BY(t.Peer.ID.DESC()).QueryContext(ctx, db, &c)
	return c.Ipv4, c.Ipv6, err
}

var (
	ErrInvalidIPv4Address = errors.New("invalid ipv4 address")
	ErrInvalidIPv6Address = errors.New("invalid ipv6 address")
)

func findNextIpv4(srcIPv4 string, serverIPv4Net *net.IPNet) (string, error) {
	ip, err := netip.ParseAddr(srcIPv4)
	if nil != err {
		return "", fmt.Errorf("unable to parse source IPv4 address: %w", err)
	}
	if !ip.Is4() || !ip.IsValid() || !ip.IsPrivate() || ip.IsLoopback() || ip.IsMulticast() || ip.IsUnspecified() {
		return "", fmt.Errorf("invalid source IPv4 address: %w", ErrInvalidIPv4Address)
	}

	nextIP := ip.Next()
	if !nextIP.IsValid() || !nextIP.IsPrivate() || nextIP.IsUnspecified() || !serverIPv4Net.Contains(nextIP.AsSlice()) {
		return "", ErrNoMoreIPv4Address
	}

	nextIPstr := nextIP.String()
	if nextIPstr == "invalid IP" {
		return "", ErrNoMoreIPv4Address
	}

	return nextIPstr, nil
}

func findNextIpv6(srcIPv6 string, serverIPv6Net *net.IPNet) (string, error) {
	ip, err := netip.ParseAddr(srcIPv6)
	if nil != err {
		return "", fmt.Errorf("unable to parse source IPv6 address: %w", err)
	}
	if !ip.Is6() || !ip.IsValid() || !ip.IsPrivate() || ip.IsLoopback() || ip.IsMulticast() || ip.IsUnspecified() {
		return "", fmt.Errorf("invalid source IPv6 address: %w", ErrInvalidIPv6Address)
	}

	nextIP := ip.Next()
	if !nextIP.IsValid() || !nextIP.IsPrivate() || nextIP.IsUnspecified() || !serverIPv6Net.Contains(nextIP.AsSlice()) {
		return "", ErrNoMoreIPv6Address
	}

	nextIPstr := nextIP.StringExpanded()
	if nextIPstr == "invalid IP" {
		return "", ErrNoMoreIPv6Address
	}

	return nextIPstr, nil
}

func (h *Handler) CreatePeer(ctx context.Context, req CreatePeerReq) (int, error) {
	lastIpv4, lastIpv6, err := getLastPeerIPs(ctx, h.db)
	if nil != err {
		if !errors.Is(err, qrm.ErrNoRows) {
			return 0, fmt.Errorf("failed to get last peer ip addresses: %v", err)
		}
		// We can ignore the case where there's no peer in the db, i.e., this is going to be the first one.
	}

	serverIPv4, serverIPv4Net, err := net.ParseCIDR(h.wgServerConf.ServerIPv4CIDR)
	if nil != err {
		return 0, err
	}
	if lastIpv4 == "" {
		lastIpv4 = serverIPv4.String()
	}

	ipv4, err := findNextIpv4(lastIpv4, serverIPv4Net)
	if nil != err {
		return 0, err
	}

	serverIPv6, serverIPv6Net, err := net.ParseCIDR(h.wgServerConf.ServerIPv6CIDR)
	if nil != err {
		return 0, err
	}
	if lastIpv6 == "" {
		lastIpv6 = serverIPv6.String()
	}

	ipv6, err := findNextIpv6(lastIpv6, serverIPv6Net)
	if nil != err {
		return 0, err
	}

	key, err := wgtypes.GeneratePrivateKey()
	if err != nil {
		return 0, err
	}

	presharedKey, err := wgtypes.GenerateKey()
	if err != nil {
		return 0, err
	}

	p := m.Peer{
		GeneratedAt:   time.Now(),
		Name:          req.Name,
		Description:   req.Description,
		GeneratedByID: uint32(req.ResellerID),
		Ipv4:          ipv4,
		Ipv6:          ipv6,
		PrivateKey:    key.String(),
		PublicKey:     key.PublicKey().String(),
		PresharedKey:  presharedKey.String(),
	}
	res, err := t.Peer.INSERT(t.Peer.MutableColumns).MODEL(p).ExecContext(ctx, h.db)
	if nil != err {
		return 0, fmt.Errorf("failed to persist new peer: %v", err)
	}

	rows, err := res.RowsAffected()
	if nil != err {
		return 0, fmt.Errorf("failed to get number of inserted peer rows: %v", err)
	} else if rows != 1 {
		return 0, fmt.Errorf("expected 1 row of peer record to be inserted, got %d", rows)
	}

	pID, err := res.LastInsertId()
	if nil != err {
		return 0, fmt.Errorf("failed to get inserted peer id: %v", err)
	}

	a := m.PeerActivation{
		At:     time.Now(),
		ByID:   uint32(req.ResellerID),
		Action: m.PeerActivationAction_Activated,
		PeerID: uint32(pID),
	}
	res, err = t.PeerActivation.INSERT(t.PeerActivation.MutableColumns).MODEL(a).ExecContext(ctx, h.db)
	if nil != err {
		return 0, fmt.Errorf("failed to insert peer activation record: %v", err)
	}

	rows, err = res.RowsAffected()
	if nil != err {
		return 0, fmt.Errorf("failed to get number of inserted peer activation rows: %v", err)
	} else if rows != 1 {
		return 0, fmt.Errorf("expected 1 row of peer activation to be inserted, got %d", rows)
	}

	if err := h.signalWGRestart(ctx); nil != err {
		return 0, err
	}

	return int(pID), nil
}

type EditPeerReq struct {
	ResellerID            int
	PeerID                int
	EditedPeerName        string
	EditedPeerDescription string
}

func (h *Handler) EditPeer(ctx context.Context, req EditPeerReq) error {
	res, err := t.Peer.
		UPDATE(t.Peer.Name, t.Peer.Description).
		SET(req.EditedPeerName, req.EditedPeerDescription).
		WHERE(
			mysql.AND(
				t.Peer.ID.EQ(mysql.Uint32(uint32(req.PeerID))),
				t.Peer.GeneratedByID.EQ(mysql.Uint32(uint32(req.ResellerID))),
				mysql.BoolExp(mysql.Func("last_insert_id", t.Peer.ID)),
			),
		).
		ExecContext(ctx, h.db)
	if nil != err {
		return fmt.Errorf("failed to persist updated peer: %v", err)
	}

	lastRecID, err := res.LastInsertId()
	if nil != err {
		return fmt.Errorf("failed to get updated record id: %v", err)
	}
	rowsNo, err := res.RowsAffected()
	if nil != err {
		return fmt.Errorf("failed to get number of rows affected by update: %v", err)
	}
	if rowsNo == 0 && lastRecID == 0 {
		return ErrPeerNotFound
	} else if rowsNo != 1 {
		return fmt.Errorf("expected 1 row of peer record to be updated, got %d", rowsNo)
	}

	return nil
}

type GetPeerConfigReq struct {
	ResellerID int
	PeerID     int
}

var (
	ErrPeerNotFound = errors.New("peer not found")
)

// GetPeerConfig returns peer configuration in the standard WireGuard ini config format.
func (h *Handler) GetPeerConfig(ctx context.Context, req GetPeerConfigReq) ([]byte, error) {
	var c m.Peer
	err := t.Peer.
		SELECT(t.Peer.Ipv4, t.Peer.Ipv6, t.Peer.PresharedKey, t.Peer.PrivateKey).
		WHERE(
			mysql.AND(
				t.Peer.ID.EQ(mysql.Uint32(uint32(req.PeerID))),
				t.Peer.GeneratedByID.EQ(mysql.Uint16(uint16(req.ResellerID))),
				mysql.BoolExp(t.PeerActivation.SELECT(t.PeerActivation.Action.EQ(mysql.NewEnumValue(m.PeerActivationAction_Activated.String()))).WHERE(t.PeerActivation.PeerID.EQ(mysql.Uint32(uint32(req.PeerID)))).LIMIT(1).ORDER_BY(t.PeerActivation.ID.DESC())),
			),
		).
		LIMIT(1).
		QueryContext(ctx, h.db, &c)
	if nil != err {
		if errors.Is(err, qrm.ErrNoRows) {
			return nil, ErrPeerNotFound
		}

		return nil, err
	}

	cfg := ini.Empty()
	ifaceSec := cfg.Section("Interface")
	ifaceSec.NewKey("PrivateKey", c.PrivateKey)
	ifaceSec.NewKey("Address", fmt.Sprintf("%s/32, %s/128", c.Ipv4, c.Ipv6))
	ifaceSec.NewKey("DNS", h.wgServerConf.PeersDNSAddress)
	peerSec := cfg.Section("Peer")
	peerSec.NewKey("PublicKey", h.wgServerConf.PublicKey)
	peerSec.NewKey("PresharedKey", c.PresharedKey)
	peerSec.NewKey("Endpoint", fmt.Sprintf("%s:%s", h.wgServerConf.PublicHost, h.wgServerConf.ListenPort))
	peerSec.NewKey("AllowedIPs", "0.0.0.0/0, ::/0")

	var out bytes.Buffer
	if _, err := cfg.WriteTo(&out); nil != err {
		return nil, fmt.Errorf("failed to serialize ini config: %w", err)
	}

	return out.Bytes(), nil
}
