package api

import (
	"context"
	"fmt"

	"gitlab.com/wireman/wireman/wgmngr/wg"
)

func (h *Handler) signalWGRestart(ctx context.Context) error {
	if err := wg.SignalRestartServer(ctx, h.db, h.wgServerConf); nil != err {
		return fmt.Errorf("failed to restart WireGuard server: %w", err)
	}

	return nil
}
