package api

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/go-jet/jet/v2/mysql"
	"github.com/go-jet/jet/v2/qrm"
	gomysql "github.com/go-sql-driver/mysql"

	m "gitlab.com/wireman/wireman/wgmngr/db/gen/latest/wgmngr/model"
	t "gitlab.com/wireman/wireman/wgmngr/db/gen/latest/wgmngr/table"
	"gitlab.com/wireman/wireman/wgmngr/password"
)

type CreateResellerReq struct {
	CreatorID                                int
	ResellerName, ResellerUsername, Password string
}

var (
	ErrDuplicateUser = errors.New("duplicate user")
)

func (h *Handler) CreateReseller(ctx context.Context, req CreateResellerReq) (int, error) {
	pwd, err := password.Hash([]byte(req.Password))
	if nil != err {
		return 0, fmt.Errorf("failed to hash reseller password: %v", err)
	}
	u := m.User{
		Name:      req.ResellerName,
		Username:  req.ResellerUsername,
		Password:  pwd,
		CreatorID: uint32(req.CreatorID),
		CreatedAt: time.Now(),
		Role:      m.UserRole_Reseller,
	}

	var duplicateUser m.User
	err = t.User.SELECT(t.User.ID).WHERE(t.User.Username.EQ(mysql.String(req.ResellerUsername))).LIMIT(1).QueryContext(ctx, h.db, &duplicateUser)
	if nil != err {
		if !errors.Is(err, qrm.ErrNoRows) {
			return 0, fmt.Errorf("failed to check dor duplicate username: %v", err)
		}
		// `err` is `qrm.ErrNoRows`, meaning there are no rows in the result, meaning there are no duplicates, meaning we can insert the user, meaning...
	}

	res, err := t.User.INSERT(t.User.MutableColumns).MODEL(u).ExecContext(ctx, h.db)
	if nil != err {
		mysqlErr := new(gomysql.MySQLError)
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 && mysqlErr.SQLState == [5]byte([]byte("23000")) && mysqlErr.Message == fmt.Sprintf("Duplicate entry '%s' for key 'user.uniq_username'", req.ResellerUsername) {
			return 0, ErrDuplicateUser
		}

		return 0, fmt.Errorf("failed to persist new reseller: %v", err)
	}

	rows, err := res.RowsAffected()
	if nil != err {
		return 0, fmt.Errorf("failed to get number of inserted reseller query result rows: %v", err)
	} else if rows != 1 {
		return 0, fmt.Errorf("expected 1 row of reseller record to be inserted, got %d", rows)
	}

	uID, err := res.LastInsertId()
	if nil != err {
		return 0, fmt.Errorf("failed to get inserted user id: %v", err)
	}

	return int(uID), nil
}
