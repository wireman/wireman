package api

import (
	"context"
	"database/sql"
	"errors"

	"github.com/go-jet/jet/v2/mysql"
	"github.com/go-jet/jet/v2/qrm"
	"github.com/rs/zerolog/log"

	m "gitlab.com/wireman/wireman/wgmngr/db/gen/latest/wgmngr/model"
	t "gitlab.com/wireman/wireman/wgmngr/db/gen/latest/wgmngr/table"
	"gitlab.com/wireman/wireman/wgmngr/password"
)

var (
	ErrUserNotFound error = errors.New("user not found")
	ErrInvalidCreds error = errors.New("username or password is invalid")
)

type userLoginCreds struct {
	id       int
	role     string
	password []byte
}

func getUserLoginInfo(ctx context.Context, db *sql.DB, username string) (*userLoginCreds, error) {
	var u m.User
	err := t.User.SELECT(t.User.ID, t.User.Role, t.User.Password).WHERE(t.User.Username.EQ(mysql.String(username))).LIMIT(1).QueryContext(ctx, db, &u)
	if nil != err {
		if errors.Is(err, qrm.ErrNoRows) {
			return nil, ErrUserNotFound
		}

		return nil, err
	}

	return &userLoginCreds{int(u.ID), u.Role.String(), u.Password}, nil
}

func (h *Handler) Login(ctx context.Context, username, passwd string) (string, error) {
	u, err := getUserLoginInfo(ctx, h.db, username)
	if nil != err {
		if errors.Is(err, ErrUserNotFound) {
			return "", err
		}

		log.Error().Err(err).Msg("failed to retrieve user stored password")
		return "", err
	}

	matches, err := password.Compare(u.password, []byte(passwd))
	if nil != err {
		log.Error().Err(err).Msg("failed to compare entered password and stored password")
		return "", err
	} else if !matches {
		return "", ErrInvalidCreds
	}

	token, err := generateToken(h.tokenSecret, TokenClaims{
		UserID:   u.id,
		Role:     u.role,
		Username: username,
	})
	if nil != err {
		log.Error().Err(err).Msg("failed to generate token")
		return "", err
	}

	return token, nil
}
