package password

import (
	"math/rand"
	"time"
)

func Sleep() {
	rand.Seed(time.Now().Unix())
	time.Sleep(time.Duration(700+rand.Int31n(300)) * time.Millisecond)
}
