//
// Code generated by go-jet DO NOT EDIT.
//
// WARNING: Changes to this file may cause incorrect behavior
// and will be lost if the code is regenerated
//

package enum

import "github.com/go-jet/jet/v2/mysql"

var PeerActivationAction = &struct {
	Activated   mysql.StringExpression
	Deactivated mysql.StringExpression
}{
	Activated:   mysql.NewEnumValue("activated"),
	Deactivated: mysql.NewEnumValue("deactivated"),
}
