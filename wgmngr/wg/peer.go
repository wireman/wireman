package wg

import (
	"bytes"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/go-jet/jet/v2/mysql"
	"gopkg.in/ini.v1"

	m "gitlab.com/wireman/wireman/wgmngr/db/gen/latest/wgmngr/model"
	t "gitlab.com/wireman/wireman/wgmngr/db/gen/latest/wgmngr/table"
)

func ReadActivePeers(ctx context.Context, db *sql.DB, conf *WGServerConfig) ([]byte, error) {
	rows, err := t.Peer.
		SELECT(t.Peer.PublicKey, t.Peer.PresharedKey, t.Peer.Ipv4, t.Peer.Ipv6, t.Peer.ID).
		WHERE(
			mysql.AND(
				mysql.BoolExp(t.PeerActivation.SELECT(t.PeerActivation.Action.EQ(mysql.NewEnumValue(m.PeerActivationAction_Activated.String()))).WHERE(t.PeerActivation.PeerID.EQ(t.Peer.ID)).LIMIT(1).ORDER_BY(t.PeerActivation.ID.DESC())),
			),
		).
		LIMIT(1_000).
		ORDER_BY(t.Peer.GeneratedAt.ASC()).
		Rows(ctx, db)
	if nil != err {
		return nil, err
	}
	defer func() {
		if closeErr := rows.Close(); nil != closeErr {
			if nil != err {
				err = fmt.Errorf("closing rows failed while there was already an error: %w", errors.Join(err, closeErr))
				return
			}

			err = closeErr
		}
	}()

	var configs []m.Peer
	for rows.Next() {
		if err := rows.Err(); nil != err {
			return nil, fmt.Errorf("failed to read the next retrieved row: %w", err)
		}

		var cfg m.Peer
		if err := rows.Scan(&cfg); nil != err {
			return nil, err
		}
		configs = append(configs, cfg)
	}

	var out bytes.Buffer
	cfg := ini.Empty()
	peerSec := cfg.Section("Interface")
	peerSec.NewKey("PrivateKey", conf.PrivateKey)
	peerSec.NewKey("ListenPort", conf.ListenPort)
	if _, err := cfg.WriteTo(&out); nil != err {
		return nil, fmt.Errorf("failed to serialize wireguard server interface section config: %w", err)
	}

	for i, c := range configs {
		cfg := ini.Empty()
		peerSec := cfg.Section("Peer")
		peerSec.NewKey("PublicKey", c.PublicKey)
		peerSec.NewKey("PresharedKey", c.PresharedKey)
		peerSec.NewKey("AllowedIPs", fmt.Sprintf("%s/32, %s/128", c.Ipv4, c.Ipv6))

		if _, err := cfg.WriteTo(&out); nil != err {
			return nil, fmt.Errorf("failed to serialize wireguard server peer %d (%d) config: %w", i, c.ID, err)
		}
	}

	return out.Bytes(), nil
}
