package wg

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"os"

	"github.com/rs/zerolog/log"
)

func SignalRestartServer(ctx context.Context, db *sql.DB, conf *WGServerConfig) error {
	peerConfigs, err := ReadActivePeers(ctx, db, conf)
	if nil != err {
		return fmt.Errorf("failed to read list of active peers: %w", err)
	}

	f, err := os.OpenFile(conf.PeersConfigFilePath, os.O_WRONLY|os.O_TRUNC|os.O_SYNC, 0755)
	if nil != err {
		if errors.Is(err, os.ErrNotExist) {
			return fmt.Errorf("destination peers config file does not exist: %w", err)
		}

		if errors.Is(err, os.ErrPermission) {
			return fmt.Errorf("permission denied on open destination peer config file request: %w", err)
		}

		log.Error().Err(err).Msg("failed to open peers config file")
		return err
	}
	defer func() {
		if closeErr := f.Close(); nil != closeErr {
			log.Error().Err(closeErr).Msg("failed to close peers config destination file")
		}
	}()

	if _, err := f.Write(peerConfigs); nil != err {
		if errors.Is(err, os.ErrPermission) {
			return fmt.Errorf("failed to write peers config to file due to permission error: %w", err)
		}

		log.Error().Err(err).Msg("failed to write peers config data")
		return err
	}

	return nil
}
