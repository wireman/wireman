package config

import (
	"bytes"
	"context"
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

type serverConfig struct {
	Domain string `yaml:"domain"`
}

type WireguardConfig struct {
	PeersDNSAddress string `yaml:"peers_dns_address"`
}

type Config struct {
	Server    serverConfig    `yaml:"server"`
	WireGuard WireguardConfig `yaml:"wireguard"`
}

func Load(ctx context.Context) (*Config, error) {
	b, err := os.ReadFile("config.yml")
	if nil != err {
		return nil, fmt.Errorf("unable to read config.yml config file contents: %w", err)
	}

	d := yaml.NewDecoder(bytes.NewBuffer(b))
	d.KnownFields(true)
	var c Config
	if err := d.Decode(&c); nil != err {
		return nil, fmt.Errorf("unable to decode config.yml contents: %v", err)
	}

	return &c, nil
}
