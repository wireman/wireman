package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"github.com/pressly/goose/v3"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.com/wireman/wireman/wgmngr/api"
	"gitlab.com/wireman/wireman/wgmngr/config"
	v1t "gitlab.com/wireman/wireman/wgmngr/db/gen/v1/wgmngr/table"
	"gitlab.com/wireman/wireman/wgmngr/env"
	"gitlab.com/wireman/wireman/wgmngr/migration"
	_ "gitlab.com/wireman/wireman/wgmngr/migration/seeds"
	"gitlab.com/wireman/wireman/wgmngr/password"
	"gitlab.com/wireman/wireman/wgmngr/wg"
)

func main() {
	ctx := context.Background()

	zerolog.TimeFieldFormat = time.RFC3339
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339})

	if len(os.Args) != 3 {
		log.Fatal().Msg("usage: <server.conf> <peers.conf>")
	}
	wgServerConfigFilePath := os.Args[1]
	wgPeersConfigFilePath := os.Args[2]

	if err := godotenv.Load(); nil != err {
		if !errors.Is(err, os.ErrNotExist) {
			log.Panic().Err(err).Msg("unexpected error while loading .env file")
		}
		log.Warn().Msg(".env file not found")
	}

	cfg, err := config.Load(ctx)
	if nil != err {
		log.Fatal().Err(err).Msg("failed to load config")
	}

	tz := env.MustGet("TZ")
	if tz != "UTC" {
		log.Fatal().Msg("TZ environment variable must be set to UTC")
	}

	dbName := env.MustGet("DB_DATABASE")
	v1t.UseSchema(dbName)

	// https://github.com/go-sql-driver/mysql/#parameters
	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s)/%s?tls=false&loc=UTC&parseTime=true",
		env.MustGet("DB_USERNAME"),
		env.MustGet("DB_PASSWORD"),
		env.MustGet("DB_ADDRESS"),
		dbName,
	)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to open database connection")
	}
	defer db.Close()

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(20)
	db.SetMaxIdleConns(10)

	if err := db.PingContext(ctx); nil != err {
		log.Fatal().Err(err).Msg("failed to ping the database")
	}

	goose.SetLogger(goose.NopLogger())
	goose.SetTableName("migrations")
	goose.SetBaseFS(migration.FS)

	if err := goose.SetDialect("mysql"); nil != err {
		log.Fatal().Err(err).Msg("failed to set goose sql dialect to mysql")
	}

	log.Trace().Msg("executing database migrations...")
	if err := goose.Up(db, "scripts"); nil != err {
		log.Fatal().Err(err).Msg("failed to run migration scripts using goose")
	}
	log.Info().Msg("database migrations executed")

	wgServerConf, err := wg.LoadConfig(ctx, wgServerConfigFilePath, wgPeersConfigFilePath, cfg.WireGuard)
	if nil != err {
		log.Fatal().Err(err).Msg("failed to load wireguard server configuration")
	}

	handler := api.NewHandler(
		[]byte(env.MustGet("AUTH_TOKEN_SECRET")),
		db,
		wgServerConf,
	)

	gin.SetMode(gin.ReleaseMode)
	engine := gin.Default()

	engine.Use(cors.New(cors.Config{
		AllowOrigins:     []string{cfg.Server.Domain},
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	router := engine.Group("api")

	router.POST("auth/login", login(&handler, cfg))
	router.GET("auth/check", isAuthenticated(&handler))
	router.PUT("auth/reset-password", resetPassword(&handler))
	router.POST("peers", createPeer(&handler))
	router.GET("peers/:id", getPeer(&handler))
	router.PUT("peers/:id", editPeer(&handler))
	router.PUT("peers/:id/deactivate", deactivatePeer(&handler))
	router.PUT("peers/:id/activate", activatePeer(&handler))
	router.GET("peers", getPeers(&handler))
	router.POST("resellers", createReseller(&handler))

	addr := ":8080"
	log.Info().Str("addr", addr).Msg("starting server...")
	if err := engine.Run(addr); nil != err {
		log.Fatal().Err(err).Msg("server stopped")
	}
}

func isMore(r io.Reader) bool {
	var buf [1]byte
	n, err := r.Read(buf[:])
	return !(errors.Is(err, io.EOF) && n == 0)
}

var (
	ErrRequestBodyTooLarge = errors.New("request body too large")
	ErrInvalidJSONBody     = errors.New("invalid json request body")
)

func readRequiredIDParam(params gin.Params) (int, bool) {
	s, ok := params.Get("id")
	if !ok {
		return 0, false
	}

	id, err := strconv.Atoi(s)
	if nil != err {
		return 0, false
	}

	return id, true
}

func parseJsonLimitedReader(limit int64, r io.ReadCloser, w http.ResponseWriter, v any) error {
	decoder := json.NewDecoder(io.LimitReader(r, limit))
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&v); nil != err {
		w.WriteHeader(http.StatusUnprocessableEntity)
		return ErrInvalidJSONBody
	}

	if isMore(r) {
		w.WriteHeader(http.StatusRequestEntityTooLarge)
		return ErrRequestBodyTooLarge
	}

	return nil
}

func login(h *api.Handler, cfg *config.Config) func(c *gin.Context) {
	const reqBodyLimit = len(`{"username":"","password":""}`) + 128 + 64

	type Form struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	validateForm := func(f *Form) error {
		if len(f.Password) == 0 {
			return errors.New("password is required")
		}

		if len(f.Password) > 128 {
			return errors.New("password cannot be longer than 128 characters")
		}

		if len(f.Username) == 0 {
			return errors.New("username is required")
		}

		if len(f.Username) != len(strings.TrimSpace(f.Username)) {
			return errors.New("invalid username")
		}

		if len(f.Username) > 64 {
			return errors.New("username cannot be longer than 64 characters")
		}

		return nil
	}

	return func(c *gin.Context) {
		defer func() {
			if err := c.Request.Body.Close(); nil != err {
				log.Error().Err(err).Msg("failed to close request body")
			}
		}()

		var f Form
		if err := parseJsonLimitedReader(int64(reqBodyLimit), c.Request.Body, c.Writer, &f); nil != err {
			return
		}

		if err := validateForm(&f); nil != err {
			c.Writer.WriteHeader(http.StatusUnprocessableEntity)
			c.Writer.WriteString(err.Error())
			return
		}

		token, err := h.Login(c.Request.Context(), f.Username, f.Password)
		if nil != err {
			if errors.Is(err, api.ErrInvalidCreds) || errors.Is(err, api.ErrUserNotFound) {
				password.Sleep()
				c.Writer.WriteHeader(http.StatusUnauthorized)
				return
			}

			log.Error().Err(err).Msg("failed to login user with unexpected error")
			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		u, err := url.Parse(cfg.Server.Domain)
		if nil != err {
			log.Error().Err(err).Msg("failed to parse configured server domain as auth cookie domain option")
			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}
		secure := false
		if u.Scheme == "https" {
			secure = true
		}

		http.SetCookie(c.Writer, &http.Cookie{
			Name:     "auth",
			Value:    url.QueryEscape(token),
			MaxAge:   int(api.AuthTokenExpDuration.Seconds()),
			Path:     "/",
			Domain:   u.Host,
			SameSite: http.SameSiteStrictMode,
			Secure:   secure,
			HttpOnly: true,
		})
		c.Writer.WriteHeader(http.StatusCreated)
	}
}

func resetPassword(h *api.Handler) func(c *gin.Context) {
	const reqBodyLimit = len(`{"currentPassword":"","newPassword":"","newPasswordConfirmation":""}`) + 3*128

	type Form struct {
		CurrentPassword         string `json:"currentPassword"`
		NewPassword             string `json:"newPassword"`
		NewPasswordConfirmation string `json:"newPasswordConfirmation"`
	}

	validateForm := func(f *Form) error {
		if len(f.CurrentPassword) == 0 {
			return errors.New("current password is required")
		}

		if len(f.CurrentPassword) > 128 {
			return errors.New("current password cannot be longer than 128 characters")
		}

		if len(f.NewPassword) == 0 {
			return errors.New("new password is required")
		}

		if len(f.NewPassword) > 128 {
			return errors.New("new password cannot be longer than 128 characters")
		}

		if len(f.NewPasswordConfirmation) == 0 {
			return errors.New("new password confirmation is required")
		}

		if len(f.NewPasswordConfirmation) > 128 {
			return errors.New("new password confirmation cannot be longer than 128 characters")
		}

		if f.NewPassword != f.NewPasswordConfirmation {
			return errors.New("new password and its confirmation must match")
		}

		return nil
	}

	return func(c *gin.Context) {
		defer func() {
			if err := c.Request.Body.Close(); nil != err {
				log.Error().Err(err).Msg("failed to close request body")
			}
		}()

		authCookie, err := c.Cookie("auth")
		if nil != err {
			if errors.Is(err, http.ErrNoCookie) {
				c.Writer.WriteHeader(http.StatusUnauthorized)
				return
			}

			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		tokenClaims, err := h.ParseVerifyToken(authCookie)
		if nil != err {
			c.Writer.WriteHeader(http.StatusUnauthorized)
			return
		}

		var f Form
		if err := parseJsonLimitedReader(int64(reqBodyLimit), c.Request.Body, c.Writer, &f); nil != err {
			return
		}

		if err := validateForm(&f); nil != err {
			c.Writer.WriteHeader(http.StatusUnprocessableEntity)
			c.Writer.WriteString(err.Error())
			return
		}

		err = h.ResetPassword(c.Request.Context(), api.ResetPasswordReq{
			UserID:          tokenClaims.UserID,
			CurrentPassword: []byte(f.CurrentPassword),
			NewPassword:     []byte(f.NewPassword),
		})
		if nil != err {
			if errors.Is(err, api.ErrInvalidCreds) || errors.Is(err, api.ErrUserNotFound) {
				password.Sleep()
				c.Writer.WriteHeader(http.StatusUnauthorized)
				return
			}

			log.Error().Err(err).Msg("failed to reset user password with unexpected error")
			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		c.Writer.WriteHeader(http.StatusNoContent)
	}
}

func createReseller(h *api.Handler) func(c *gin.Context) {
	const reqBodyLimit = len(`{"name":"","username":"","password":""}`) + 64 + 256 + 128

	type Form struct {
		ResellerName     string `json:"name"`
		ResellerUsername string `json:"username"`
		ResellerPassword string `json:"password"`
	}

	validateForm := func(f *Form) error {
		if len(f.ResellerName) == 0 {
			return errors.New("reseller name is required")
		}

		if len(f.ResellerName) > 256 {
			return errors.New("reseller name cannot be longer than 256 characters")
		}

		if len(f.ResellerPassword) == 0 {
			return errors.New("reseller password is required")
		}

		if len(f.ResellerPassword) > 128 {
			return errors.New("reseller password cannot be longer than 128 characters")
		}

		if len(f.ResellerUsername) == 0 {
			return errors.New("reseller username is required")
		}

		if len(f.ResellerUsername) > 128 {
			return errors.New("reseller username cannot be longer than 64 characters")
		}

		return nil
	}

	return func(c *gin.Context) {
		defer func() {
			if err := c.Request.Body.Close(); nil != err {
				log.Error().Err(err).Msg("failed to close request body")
			}
		}()

		authCookie, err := c.Cookie("auth")
		if nil != err {
			if errors.Is(err, http.ErrNoCookie) {
				c.Writer.WriteHeader(http.StatusUnauthorized)
				return
			}

			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		tokenClaims, err := h.ParseVerifyToken(authCookie)
		if nil != err {
			c.Writer.WriteHeader(http.StatusUnauthorized)
			return
		}
		if tokenClaims.Role != "admin" {
			c.Writer.WriteHeader(http.StatusForbidden)
			return
		}

		var f Form
		if err := parseJsonLimitedReader(int64(reqBodyLimit), c.Request.Body, c.Writer, &f); nil != err {
			return
		}

		if err := validateForm(&f); nil != err {
			c.Writer.WriteHeader(http.StatusUnprocessableEntity)
			c.Writer.WriteString(err.Error())
			return
		}

		resellerID, err := h.CreateReseller(c.Request.Context(), api.CreateResellerReq{
			CreatorID:        tokenClaims.UserID,
			ResellerName:     f.ResellerName,
			ResellerUsername: f.ResellerUsername,
			Password:         f.ResellerPassword,
		})
		if nil != err {
			if errors.Is(err, api.ErrDuplicateUser) {
				c.Writer.WriteHeader(http.StatusUnprocessableEntity)
				c.Writer.WriteString("duplicate username")
				return
			}

			log.Error().Err(err).Msg("failed to create reseller user with unexpected error")
			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		c.Header("Content-Type", "text/plain; charset=utf-8")
		c.Writer.WriteHeader(http.StatusCreated)
		c.Writer.WriteString(fmt.Sprintf("%d", resellerID))
	}
}

func createPeer(h *api.Handler) func(c *gin.Context) {
	const reqBodyLimit = len(`{"name":"","description":""}`) + 256 + 10_000

	type Form struct {
		Name        string `json:"name"`
		Description string `json:"description"`
	}

	validateForm := func(f *Form) error {
		if len(f.Name) == 0 {
			return errors.New("name is required")
		}

		if len(f.Name) > 256 {
			return errors.New("name cannot be longer than 256 characters")
		}

		if len(f.Name) != len(strings.TrimSpace(f.Name)) {
			return errors.New("invalid name")
		}

		if len(f.Description) > 10000 {
			return errors.New("description cannot be longer than 10,000 characters")
		}

		if len(f.Description) != len(strings.TrimSpace(f.Description)) {
			return errors.New("invalid description")
		}

		return nil
	}

	return func(c *gin.Context) {
		defer func() {
			if err := c.Request.Body.Close(); nil != err {
				log.Error().Err(err).Msg("failed to close request body")
			}
		}()

		authCookie, err := c.Cookie("auth")
		if nil != err {
			if errors.Is(err, http.ErrNoCookie) {
				c.Writer.WriteHeader(http.StatusUnauthorized)
				return
			}

			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		tokenClaims, err := h.ParseVerifyToken(authCookie)
		if nil != err {
			c.Writer.WriteHeader(http.StatusUnauthorized)
			return
		}

		var f Form
		if err := parseJsonLimitedReader(int64(reqBodyLimit), c.Request.Body, c.Writer, &f); nil != err {
			return
		}

		if err := validateForm(&f); nil != err {
			c.Writer.WriteHeader(http.StatusUnprocessableEntity)
			c.Writer.WriteString(err.Error())
			return
		}

		peerID, err := h.CreatePeer(c.Request.Context(), api.CreatePeerReq{
			Name:        f.Name,
			Description: f.Description,
			ResellerID:  tokenClaims.UserID,
		})
		if nil != err {
			if errors.Is(err, api.ErrInvalidIPv4Address) || errors.Is(err, api.ErrInvalidIPv6Address) {
				log.Error().Err(err).Msg("invalid ip address")
				c.Writer.WriteHeader(http.StatusInternalServerError)
				return
			}
			if errors.Is(err, api.ErrNoMoreIPv4Address) || errors.Is(err, api.ErrNoMoreIPv6Address) {
				log.Error().Err(err).Msg("no more ip address to associate")
				c.Writer.WriteHeader(http.StatusInternalServerError)
				return
			}

			log.Error().Err(err).Msg("failed to create peer with an unexpected error")
			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		c.Header("Content-Type", "text/plain; charset=utf-8")
		c.Writer.WriteHeader(http.StatusCreated)
		c.Writer.WriteString(fmt.Sprintf("%d", peerID))
	}
}

func getPeer(h *api.Handler) func(c *gin.Context) {
	return func(c *gin.Context) {
		if err := c.Request.Body.Close(); nil != err {
			c.Writer.WriteHeader(http.StatusInternalServerError)
			log.Error().Err(err).Msg("failed to close request body")
		}

		authCookie, err := c.Cookie("auth")
		if nil != err {
			if errors.Is(err, http.ErrNoCookie) {
				c.Writer.WriteHeader(http.StatusUnauthorized)
				return
			}

			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		tokenClaims, err := h.ParseVerifyToken(authCookie)
		if nil != err {
			c.Writer.WriteHeader(http.StatusUnauthorized)
			return
		}

		id, ok := readRequiredIDParam(c.Params)
		if !ok {
			c.Writer.WriteHeader(http.StatusUnprocessableEntity)
			return
		}

		configContent, err := h.GetPeerConfig(c.Request.Context(), api.GetPeerConfigReq{
			ResellerID: tokenClaims.UserID,
			PeerID:     id,
		})
		if nil != err {
			if errors.Is(err, api.ErrPeerNotFound) {
				c.Writer.WriteHeader(http.StatusNotFound)
				return
			}

			log.Error().Err(err).Msg("failed to get peer config with unexpected error")
			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		c.Header("Content-Type", "text/plain; charset=utf-8")
		c.Writer.WriteHeader(http.StatusOK)
		c.Writer.Write(configContent)
	}
}

func deactivatePeer(h *api.Handler) func(c *gin.Context) {
	return func(c *gin.Context) {
		if err := c.Request.Body.Close(); nil != err {
			c.Writer.WriteHeader(http.StatusInternalServerError)
			log.Error().Err(err).Msg("failed to close request body")
		}

		authCookie, err := c.Cookie("auth")
		if nil != err {
			if errors.Is(err, http.ErrNoCookie) {
				c.Writer.WriteHeader(http.StatusUnauthorized)
				return
			}

			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		tokenClaims, err := h.ParseVerifyToken(authCookie)
		if nil != err {
			c.Writer.WriteHeader(http.StatusUnauthorized)
			return
		}

		id, ok := readRequiredIDParam(c.Params)
		if !ok {
			c.Writer.WriteHeader(http.StatusUnprocessableEntity)
			return
		}

		err = h.DeactivatePeer(c.Request.Context(), id, tokenClaims.UserID)
		if nil != err {
			if errors.Is(err, api.ErrPeerAlreadyDeactivated) {
				c.Writer.WriteHeader(http.StatusNoContent)
				return
			}
			if errors.Is(err, api.ErrPeerNotFound) {
				c.Writer.WriteHeader(http.StatusNotFound)
				return
			}

			log.Error().Err(err).Msg("failed to deactivate peer")
			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		c.Writer.WriteHeader(http.StatusNoContent)
	}
}

func activatePeer(h *api.Handler) func(c *gin.Context) {
	return func(c *gin.Context) {
		if err := c.Request.Body.Close(); nil != err {
			c.Writer.WriteHeader(http.StatusInternalServerError)
			log.Error().Err(err).Msg("failed to close request body")
		}

		authCookie, err := c.Cookie("auth")
		if nil != err {
			if errors.Is(err, http.ErrNoCookie) {
				c.Writer.WriteHeader(http.StatusUnauthorized)
				return
			}

			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		tokenClaims, err := h.ParseVerifyToken(authCookie)
		if nil != err {
			c.Writer.WriteHeader(http.StatusUnauthorized)
			return
		}

		id, ok := readRequiredIDParam(c.Params)
		if !ok {
			c.Writer.WriteHeader(http.StatusUnprocessableEntity)
			return
		}

		err = h.ActivatePeer(c.Request.Context(), id, tokenClaims.UserID)
		if nil != err {
			if errors.Is(err, api.ErrPeerAlreadyActivated) {
				c.Writer.WriteHeader(http.StatusNoContent)
				return
			}
			if errors.Is(err, api.ErrPeerNotFound) {
				c.Writer.WriteHeader(http.StatusNotFound)
				return
			}

			log.Error().Err(err).Msg("failed to activate peer with unexpected error")
			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		c.Writer.WriteHeader(http.StatusNoContent)
	}
}

func editPeer(h *api.Handler) func(c *gin.Context) {
	const reqBodyLimit = len(`{"name":"","description":""}`) + 256 + 10_000

	type Form struct {
		Name        string `json:"name"`
		Description string `json:"description"`
	}

	validateForm := func(f *Form) error {
		if len(f.Name) == 0 {
			return errors.New("name is required")
		}

		if len(f.Name) > 256 {
			return errors.New("name cannot be longer than 256 characters")
		}

		if len(f.Name) != len(strings.TrimSpace(f.Name)) {
			return errors.New("invalid name")
		}

		if len(f.Description) > 10000 {
			return errors.New("description cannot be longer than 10,000 characters")
		}

		if len(f.Description) != len(strings.TrimSpace(f.Description)) {
			return errors.New("invalid description")
		}

		return nil
	}

	return func(c *gin.Context) {
		defer func() {
			if err := c.Request.Body.Close(); nil != err {
				log.Error().Err(err).Msg("failed to close request body")
			}
		}()

		authCookie, err := c.Cookie("auth")
		if nil != err {
			if errors.Is(err, http.ErrNoCookie) {
				c.Writer.WriteHeader(http.StatusUnauthorized)
				return
			}

			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		tokenClaims, err := h.ParseVerifyToken(authCookie)
		if nil != err {
			c.Writer.WriteHeader(http.StatusUnauthorized)
			return
		}

		var f Form
		if err := parseJsonLimitedReader(int64(reqBodyLimit), c.Request.Body, c.Writer, &f); nil != err {
			return
		}

		if err := validateForm(&f); nil != err {
			c.Writer.WriteHeader(http.StatusUnprocessableEntity)
			c.Writer.WriteString(err.Error())
			return
		}

		id, ok := readRequiredIDParam(c.Params)
		if !ok {
			c.Writer.WriteHeader(http.StatusUnprocessableEntity)
			return
		}

		err = h.EditPeer(c.Request.Context(), api.EditPeerReq{
			ResellerID:            tokenClaims.UserID,
			PeerID:                id,
			EditedPeerName:        f.Name,
			EditedPeerDescription: f.Description,
		})
		if nil != err {
			if errors.Is(err, api.ErrPeerNotFound) {
				c.Writer.WriteHeader(http.StatusNotFound)
				return
			}

			log.Error().Err(err).Msg("failed to edit peer with unexpected error")
			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		c.Writer.WriteHeader(http.StatusNoContent)
	}
}

func getPeers(h *api.Handler) func(c *gin.Context) {
	return func(c *gin.Context) {
		if err := c.Request.Body.Close(); nil != err {
			c.Writer.WriteHeader(http.StatusInternalServerError)
			log.Error().Err(err).Msg("failed to close request body")
		}

		configsContent, err := h.GetActivePeerConfigs(c.Request.Context())
		if nil != err {
			log.Error().Err(err).Msg("failed to get peer configs with unexpected error")
			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		c.Header("Content-Type", "text/plain; charset=utf-8")
		c.Writer.WriteHeader(http.StatusOK)
		c.Writer.Write(configsContent)
	}
}

func isAuthenticated(h *api.Handler) func(c *gin.Context) {
	return func(c *gin.Context) {
		if err := c.Request.Body.Close(); nil != err {
			c.Writer.WriteHeader(http.StatusInternalServerError)
			log.Error().Err(err).Msg("failed to close request body")
		}

		authCookie, err := c.Cookie("auth")
		if nil != err {
			if errors.Is(err, http.ErrNoCookie) {
				c.Writer.WriteHeader(http.StatusUnauthorized)
				return
			}

			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		}

		ok, err := h.IsAuthenticated(c.Request.Context(), authCookie)
		if nil != err {
			c.Writer.WriteHeader(http.StatusInternalServerError)
			return
		} else if !ok {
			c.Writer.WriteHeader(http.StatusUnauthorized)
			return
		}

		c.Writer.WriteHeader(http.StatusOK)
	}
}
