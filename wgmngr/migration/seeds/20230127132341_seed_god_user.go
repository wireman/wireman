package migrations

import (
	"database/sql"
	"errors"
	"time"

	"github.com/go-jet/jet/v2/mysql"
	"github.com/pressly/goose/v3"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	m "gitlab.com/wireman/wireman/wgmngr/db/gen/v1/wgmngr/model"
	t "gitlab.com/wireman/wireman/wgmngr/db/gen/v1/wgmngr/table"
	"gitlab.com/wireman/wireman/wgmngr/env"
	"gitlab.com/wireman/wireman/wgmngr/password"
)

func init() {
	goose.AddMigration(upSeedGodUser, downSeedGodUser)
}

func upSeedGodUser(tx *sql.Tx) error {
	godPassword := env.MustGet("GOD_PASSWORD")
	if !password.StrongPasswordRegExp.MatchString(godPassword) {
		return errors.New("GO_PASSWORD environment variable is not strong enough")
	}

	hash, err := password.Hash([]byte(godPassword))
	if nil != err {
		return err
	}

	godUser := m.User{
		Name:      "God",
		Username:  "god",
		Password:  hash,
		CreatorID: 1,
		CreatedAt: time.Now(),
		Role:      m.UserRole_Admin,
	}
	log.Debug().Dict("god", zerolog.Dict().Time("created-at", godUser.CreatedAt)).Msg("seeding god user")

	if _, err := tx.Exec("SET FOREIGN_KEY_CHECKS=0;"); nil != err {
		return err
	}
	defer func() {
		if _, err := tx.Exec("SET FOREIGN_KEY_CHECKS=1;"); nil != err {
			log.Warn().Err(err).Msg("failed to revert FOREIGN_KEY_CHECKS database option to 1")
		}
	}()

	res, err := t.User.INSERT(t.User.MutableColumns).MODEL(godUser).Exec(tx)
	if nil != err {
		return err
	}
	rows, err := res.RowsAffected()
	if nil != err {
		return err
	}
	if rows != 1 {
		return errors.New("god user was not inserted")
	}

	return err
}

func downSeedGodUser(tx *sql.Tx) error {
	res, err := t.User.DELETE().WHERE(t.User.Username.EQ(mysql.String("god"))).Exec(tx)
	if nil != err {
		return err
	}
	rows, err := res.RowsAffected()
	if nil != err {
		return err
	}
	if rows != 1 {
		log.Warn().Msg("expected god user to be inserted but seems it's already deleted")
	}

	return nil
}
