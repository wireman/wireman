-- +goose Up
CREATE TABLE `user` (
  `id` INT(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(256) NOT NULL,
  `username` VARCHAR(64) NOT NULL,
  `password` VARBINARY(10000) NOT NULL,
  `creator_id` INT(32) UNSIGNED NOT NULL,
  `created_at` DATETIME NOT NULL,
  `role` ENUM('admin', 'reseller') NOT NULL,
  CONSTRAINT pk_id PRIMARY KEY USING BTREE (`id`),
  CONSTRAINT uniq_id UNIQUE INDEX USING BTREE (`id`),
  CONSTRAINT uniq_username UNIQUE INDEX USING HASH (`username`),
  FULLTEXT full_text_name (`name`),
  CONSTRAINT fk_creator_id FOREIGN KEY fk_creator_by_id (`creator_id`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB;

-- +goose Down
DROP TABLE `user`;
