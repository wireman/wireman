-- +goose Up
CREATE TABLE `peer_activation` (
  `id` INT(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `at` DATETIME NOT NULL,
  `by_id` INT(32) UNSIGNED NOT NULL,
  `peer_id` INT(32) UNSIGNED NOT NULL,
  `action` ENUM('activated', 'deactivated') NOT NULL,
  CONSTRAINT pk_id PRIMARY KEY USING BTREE (`id`),
  CONSTRAINT fk_by_id FOREIGN KEY fk_idx_by_id (`by_id`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT fk_peer_id FOREIGN KEY fk_idx_peer_id (`peer_id`) REFERENCES `peer`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB;

-- +goose Down
DROP TABLE `peer_activation`;
