-- +goose Up
CREATE TABLE `peer` (
  `id` INT(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(256) NOT NULL,
  `description` VARCHAR(10000) NOT NULL,
  `generated_by_id` INT(32) UNSIGNED NOT NULL,
  `generated_at` DATETIME NOT NULL,
  `ipv4` VARCHAR(15) NOT NULL,
  `ipv6` VARCHAR(39) NOT NULL,
  `private_key` CHAR(44) NOT NULL,
  `public_key` CHAR(44) NOT NULL,
  `preshared_key` CHAR(44) NOT NULL,
  CONSTRAINT pk_id PRIMARY KEY USING BTREE (`id`),
  CONSTRAINT uniq_id UNIQUE INDEX USING BTREE (`id`),
  CONSTRAINT uniq_idx_ipv4 UNIQUE INDEX USING HASH (`ipv4`),
  CONSTRAINT uniq_idx_ipv6 UNIQUE INDEX USING HASH (`ipv6`),
  CONSTRAINT uniq_idx_private_key UNIQUE INDEX USING HASH (`private_key`),
  CONSTRAINT uniq_idx_public_key UNIQUE INDEX USING HASH (`public_key`),
  CONSTRAINT uniq_idx_preshared_key UNIQUE INDEX USING HASH (`preshared_key`),
  FULLTEXT name_full_text (`name`),
  FULLTEXT description_full_text (`description`),
  CONSTRAINT fk_generated_by_id FOREIGN KEY fk_generated_by_id (`generated_by_id`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB;

-- +goose Down
DROP TABLE `peer`;
